coordinate_z = (230, 460)
coordinate_x = (295, 460)
coordinate_c = (360, 460)
coordinate_v = (430, 460)
coordinate_b = (500, 460)
coordinate_n = (570, 460)
coordinate_m = (640, 460)
coordinate_j = (605, 410)
coordinate_k = (675, 410)
coordinate_l = (745, 410)
coordinate_h = (535, 410)
coordinate_g = (465, 410)
coordinate_f = (395, 410)
coordinate_d = (325, 410)
coordinate_s = (260, 410)
coordinate_a = (190, 410)
coordinate_e = (315, 360)
coordinate_w = (245, 360)
coordinate_q = (175, 360)
coordinate_r = (380, 360)
coordinate_t = (450, 360)
coordinate_y = (520, 360)
coordinate_u = (585, 360)
coordinate_i = (660, 360)
coordinate_o = (730, 360)
coordinate_p = (795, 360)
coordinate_space = (500, 515)
coordinate_enter = (1010, 370)
coordinate_1 = (140, 310)
coordinate_2 = (205, 310)
coordinate_3 = (275, 310)
coordinate_4 = (345, 310)
coordinate_5 = (415, 310)
coordinate_6 = (485, 310)
coordinate_7 = (550, 310)
coordinate_8 = (620, 310)
coordinate_9 = (695, 310)
coordinate_0 = (760, 310)

all_coordinates = {'a': coordinate_a,
                   'b': coordinate_b,
                   'c': coordinate_c,
                   'd': coordinate_d,
                   'e': coordinate_e,
                   'f': coordinate_f,
                   'g': coordinate_g,
                   'h': coordinate_h,
                   'i': coordinate_i,
                   'j': coordinate_j,
                   'k': coordinate_k,
                   'l': coordinate_l,
                   'm': coordinate_m,
                   'n': coordinate_n,
                   'o': coordinate_o,
                   'p': coordinate_p,
                   'q': coordinate_q,
                   'r': coordinate_r,
                   's': coordinate_s,
                   't': coordinate_t,
                   'u': coordinate_u,
                   'v': coordinate_v,
                   'w': coordinate_w,
                   'x': coordinate_x,
                   'y': coordinate_y,
                   'z': coordinate_z,
                   ' ': coordinate_space,
                   '\n': coordinate_enter,
                   '1': coordinate_1,
                   '2': coordinate_2,
                   '3': coordinate_3,
                   '4': coordinate_4,
                   '5': coordinate_5,
                   '6': coordinate_6,
                   '7': coordinate_7,
                   '8': coordinate_8,
                   '9': coordinate_9,
                   '0': coordinate_0}

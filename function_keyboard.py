import time
import coordinates
import pygame
from globals import RED, WHITE, BLACK, BLUE, WINDOW_SIZE, KEYBOARD_CENTER, FPS, SPACE, STR_OUTPUT


def get_coordinate(symbol=str):
    if symbol.isalpha() and symbol.isupper():
        return coordinates.all_coordinates[symbol.lower()]
    return coordinates.all_coordinates[symbol]


with open("text.txt", "r") as f_read:
    global STR_OUTPUT
    for string in f_read:
        STR_OUTPUT += string


def start_program():
    pygame.init()
    pygame.display.set_caption("Keyboard Simulator")
    screen = pygame.display.set_mode(WINDOW_SIZE)
    clock = pygame.time.Clock()

    screen.fill(RED)

    back_image = pygame.image.load('Images/keyboard.jpg')
    back_image.convert()

    rect = back_image.get_rect()
    rect.center = KEYBOARD_CENTER
    screen.blit(back_image, rect)

    # текст 26 symbols 72 size
    fnt_text = pygame.font.Font(pygame.font.get_default_font(), 72)
    fnt_space = pygame.font.Font(pygame.font.get_default_font(), 72)
    fnt_object_count_error = pygame.font.Font(pygame.font.get_default_font(), 30)
    fnt_object_speed = pygame.font.Font(pygame.font.get_default_font(), 30)

    pygame.display.update()
    global STR_OUTPUT
    count_errors = 0
    count_true = 0
    count_true_last = 0

    start = time.time()

    delta = 0
    running = True
    while running:
        back_image = pygame.image.load('Images/keyboard.jpg')
        back_image.convert()
        rect = back_image.get_rect()
        rect.center = KEYBOARD_CENTER
        screen.blit(back_image, rect)

        if STR_OUTPUT[0].isalpha() and STR_OUTPUT[0].isupper():
            pygame.draw.ellipse(screen, BLUE, (883, 415, 160, 80), 3)

        pygame.draw.circle(screen, BLUE, get_coordinate(STR_OUTPUT[0]), 40, 2)
        text = fnt_text.render(STR_OUTPUT, 1, WHITE, BLACK)
        white_text = fnt_space.render(SPACE, 0, WHITE, BLACK)
        c_err = fnt_object_count_error.render("count_errors: " + str(count_errors), WHITE, BLACK)

        if time.time() - start > 5:
            start = time.time()
            count_true_last = count_true

        speed_text = "chars in the last five seconds:  " + str(count_true - count_true_last)
        speed_obj = fnt_object_speed.render(speed_text, WHITE, BLACK)
        screen.blit(speed_obj, (500, 200))

        screen.blit(white_text, (0, 0))
        screen.blit(text, (0, 0))
        screen.blit(c_err, (100, 200))

        autumn_first = pygame.image.load('Images/autumn.jpg')
        autumn_first.convert()
        rect = autumn_first.get_rect()
        rect.center = (270 - delta % 1080, 125)
        screen.blit(autumn_first, rect)

        winter_first = pygame.image.load('Images/winter.jpg')
        winter_first.convert()
        rect = winter_first.get_rect()
        rect.center = (810 - delta % 1080, 125)
        screen.blit(winter_first, rect)

        spring = pygame.image.load('Images/spring.jpg')
        spring.convert()
        rect = spring.get_rect()
        rect.center = (1350 - delta % 2160, 125)
        screen.blit(spring, rect)

        summer = pygame.image.load('Images/summer.jpg')
        summer.convert()
        rect = summer.get_rect()
        rect.center = (1890 - delta % 2160, 125)
        screen.blit(summer, rect)

        autumn_second = pygame.image.load('Images/autumn.jpg')
        autumn_second.convert()
        rect = autumn_second.get_rect()
        rect.center = (2430 - delta % 2160, 125)
        screen.blit(autumn_second, rect)

        winter_second = pygame.image.load('Images/winter.jpg')
        winter_second.convert()
        rect = winter_second.get_rect()
        rect.center = (2970 - delta % 2160, 125)
        screen.blit(winter_second, rect)

        clock.tick(FPS)
        for event in pygame.event.get():
            if len(STR_OUTPUT) == 0:
                running = False
            if event.type == pygame.QUIT:
                running = False

            if event.type == pygame.KEYDOWN:
                current_symbol = STR_OUTPUT[0]
                # enter
                if current_symbol == '\n':
                    if event.key == pygame.K_RETURN:
                        count_true += 1
                        delta += 10
                        STR_OUTPUT = STR_OUTPUT[1:]
                    else:
                        error_image = pygame.image.load('Images/error.jpg')
                        error_image.convert()
                        rect = error_image.get_rect()
                        rect.center = (540, 300)
                        screen.blit(error_image, rect)
                        count_errors += 1
                    pygame.display.update()
                    continue
                # symbol with shift
                if current_symbol.isalpha() and current_symbol.isupper():
                    keys = pygame.key.get_pressed()
                    current_symbol = current_symbol.lower()
                    if keys[pygame.K_LSHIFT] or keys[pygame.K_RSHIFT]:
                        if keys[ord(current_symbol)]:
                            count_true += 1
                            delta += 10
                            STR_OUTPUT = STR_OUTPUT[1:]
                            pygame.display.update()
                            continue
                        if keys.count(True) > 1:
                            error_image = pygame.image.load('Images/error.jpg')
                            error_image.convert()
                            rect = error_image.get_rect()
                            rect.center = (540, 300)
                            screen.blit(error_image, rect)
                            count_errors += 1
                            pygame.display.update()
                            continue
                        else:
                            pygame.display.update()
                            continue
                    else:
                        error_image = pygame.image.load('Images/error.jpg')
                        error_image.convert()
                        rect = error_image.get_rect()
                        rect.center = (540, 300)
                        screen.blit(error_image, rect)
                        count_errors += 1
                        pygame.display.update()
                        continue
                # lower alpfa
                if event.key == ord(current_symbol):
                    count_true += 1
                    delta += 10
                    STR_OUTPUT = STR_OUTPUT[1:]
                else:
                    error_image = pygame.image.load('Images/error.jpg')
                    error_image.convert()
                    rect = error_image.get_rect()
                    rect.center = (540, 300)
                    screen.blit(error_image, rect)
                    count_errors += 1
        pygame.display.update()

    pygame.quit()
